/*
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of vTools.

vTools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vTools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with vTools.  If not, see <https://www.gnu.org/licenses/>.
*/

-- colors
vG = color 68 68 68
vU = color 100 100 100
vB = color 50 200 255
vS = color 255 204 153
vO = color 255 127 0
vW = color 255 255 255
vC = color 0 255 255
vV = color 98 222 49
vP = color 253 138 193
vD = color 56 56 56
vF = color 81 81 81
vM = color 95 138 193
vR = color 255 0 0
vN = color 0 0 0

SelectOppositeBitmap = bitmap 20 20 color:vU (
	-- Soft corners
	setPixels SelectOppositeBitmap [0, 0] #(vG, vG)
	setPixels SelectOppositeBitmap [0, 1] #(vG)
	setPixels SelectOppositeBitmap [18, 0] #(vG, vG)
	setPixels SelectOppositeBitmap [19, 1] #(vG)
	setPixels SelectOppositeBitmap [0, 19] #(vG, vG)
	setPixels SelectOppositeBitmap [0, 18] #(vG)
	setPixels SelectOppositeBitmap [18, 19] #(vG, vG)
	setPixels SelectOppositeBitmap [19, 18] #(vG)
	-- graphics
	setPixels SelectOppositeBitmap [3, 8] #(vS, vS, vS, vU, vU, vU, vU, vU, vU, vU, vU, vO, vO, vO)
	setPixels SelectOppositeBitmap [2, 9] #(vS, vS, vS, vS, vS, vU, vU, vU, vW, vU, vU, vO, vO, vO, vO, vO)
	setPixels SelectOppositeBitmap [2, 10] #(vS, vS, vS, vS, vS, vU, vW, vW, vW, vW, vU, vO, vO, vO, vO, vO)
	setPixels SelectOppositeBitmap [2, 11] #(vS, vS, vS, vS, vS, vU, vU, vU, vW, vU, vU, vO, vO, vO, vO, vO)
	setPixels SelectOppositeBitmap [3, 12] #(vS, vS, vS, vU, vU, vU, vU, vU, vU, vU, vU, vO, vO, vO)
)
OverSelectOppositeBitmap = bitmap 20 20 color:vD (
	-- Soft corners
	setPixels OverSelectOppositeBitmap [0, 0] #(vG, vG)
	setPixels OverSelectOppositeBitmap [0, 1] #(vG)
	setPixels OverSelectOppositeBitmap [18, 0] #(vG, vG)
	setPixels OverSelectOppositeBitmap [19, 1] #(vG)
	setPixels OverSelectOppositeBitmap [0, 19] #(vG, vG)
	setPixels OverSelectOppositeBitmap [0, 18] #(vG)
	setPixels OverSelectOppositeBitmap [18, 19] #(vG, vG)
	setPixels OverSelectOppositeBitmap [19, 18] #(vG)
	-- graphics
	setPixels OverSelectOppositeBitmap [3, 8] #(vS, vS, vS, vD, vD, vD, vD, vD, vD, vD, vD, vO, vO, vO)
	setPixels OverSelectOppositeBitmap [2, 9] #(vS, vS, vS, vS, vS, vD, vU, vU, vW, vD, vD, vO, vO, vO, vO, vO)
	setPixels OverSelectOppositeBitmap [2, 10] #(vS, vS, vS, vS, vS, vD, vW, vW, vW, vW, vD, vO, vO, vO, vO, vO)
	setPixels OverSelectOppositeBitmap [2, 11] #(vS, vS, vS, vS, vS, vD, vU, vU, vW, vD, vD, vO, vO, vO, vO, vO)
	setPixels OverSelectOppositeBitmap [3, 12] #(vS, vS, vS, vD, vD, vD, vD, vD, vD, vD, vD, vO, vO, vO)
)
ClickSelectOppositeBitmap = bitmap 20 20 color:vM (
	-- Soft corners
	setPixels ClickSelectOppositeBitmap [0, 0] #(vG, vG)
	setPixels ClickSelectOppositeBitmap [0, 1] #(vG)
	setPixels ClickSelectOppositeBitmap [18, 0] #(vG, vG)
	setPixels ClickSelectOppositeBitmap [19, 1] #(vG)
	setPixels ClickSelectOppositeBitmap [0, 19] #(vG, vG)
	setPixels ClickSelectOppositeBitmap [0, 18] #(vG)
	setPixels ClickSelectOppositeBitmap [18, 19] #(vG, vG)
	setPixels ClickSelectOppositeBitmap [19, 18] #(vG)
	-- graphics
	setPixels ClickSelectOppositeBitmap [3, 8] #(vS, vS, vS, vM, vM, vM, vM, vM, vM, vM, vM, vO, vO, vO)
	setPixels ClickSelectOppositeBitmap [2, 9] #(vS, vS, vS, vS, vS, vM, vM, vM, vW, vM, vM, vO, vO, vO, vO, vO)
	setPixels ClickSelectOppositeBitmap [2, 10] #(vS, vS, vS, vS, vS, vM, vW, vW, vW, vW, vM, vO, vO, vO, vO, vO)
	setPixels ClickSelectOppositeBitmap [2, 11] #(vS, vS, vS, vS, vS, vM, vM, vM, vW, vM, vM, vO, vO, vO, vO, vO)
	setPixels ClickSelectOppositeBitmap [3, 12] #(vS, vS, vS, vM, vM, vM, vM, vM, vM, vM, vM, vO, vO, vO)
)

SelectmoreOppositeBitmap = bitmap 20 20 color:vU (
	-- Soft corners
	setPixels SelectmoreOppositeBitmap [0, 0] #(vG, vG)
	setPixels SelectmoreOppositeBitmap [0, 1] #(vG)
	setPixels SelectmoreOppositeBitmap [18, 0] #(vG, vG)
	setPixels SelectmoreOppositeBitmap [19, 1] #(vG)
	setPixels SelectmoreOppositeBitmap [0, 19] #(vG, vG)
	setPixels SelectmoreOppositeBitmap [0, 18] #(vG)
	setPixels SelectmoreOppositeBitmap [18, 19] #(vG, vG)
	setPixels SelectmoreOppositeBitmap [19, 18] #(vG)
	-- graphics
	setPixels SelectmoreOppositeBitmap [3, 8] #(vS, vS, vS, vU, vU, vU, vU, vU, vU, vU, vU, vS, vS, vS)
	setPixels SelectmoreOppositeBitmap [2, 9] #(vS, vS, vS, vS, vS, vU, vU, vU, vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels SelectmoreOppositeBitmap [2, 10] #(vS, vS, vS, vS, vS, vU, vW, vW, vW, vW, vU, vS, vS, vS, vS, vS)
	setPixels SelectmoreOppositeBitmap [2, 11] #(vS, vS, vS, vS, vS, vU, vU, vU, vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels SelectmoreOppositeBitmap [3, 12] #(vS, vS, vS, vU, vU, vU, vU, vU, vU, vU, vU, vS, vS, vS)
)
OverSelectmoreOppositeBitmap = bitmap 20 20 color:vD (
	-- Soft corners
	setPixels OverSelectmoreOppositeBitmap [0, 0] #(vG, vG)
	setPixels OverSelectmoreOppositeBitmap [0, 1] #(vG)
	setPixels OverSelectmoreOppositeBitmap [18, 0] #(vG, vG)
	setPixels OverSelectmoreOppositeBitmap [19, 1] #(vG)
	setPixels OverSelectmoreOppositeBitmap [0, 19] #(vG, vG)
	setPixels OverSelectmoreOppositeBitmap [0, 18] #(vG)
	setPixels OverSelectmoreOppositeBitmap [18, 19] #(vG, vG)
	setPixels OverSelectmoreOppositeBitmap [19, 18] #(vG)
	-- graphics
	setPixels OverSelectmoreOppositeBitmap [3, 8] #(vS, vS, vS, vD, vD, vD, vD, vD, vD, vD, vD, vS, vS, vS)
	setPixels OverSelectmoreOppositeBitmap [2, 9] #(vS, vS, vS, vS, vS, vD, vU, vU, vW, vD, vD, vS, vS, vS, vS, vS)
	setPixels OverSelectmoreOppositeBitmap [2, 10] #(vS, vS, vS, vS, vS, vD, vW, vW, vW, vW, vD, vS, vS, vS, vS, vS)
	setPixels OverSelectmoreOppositeBitmap [2, 11] #(vS, vS, vS, vS, vS, vD, vU, vU, vW, vD, vD, vS, vS, vS, vS, vS)
	setPixels OverSelectmoreOppositeBitmap [3, 12] #(vS, vS, vS, vD, vD, vD, vD, vD, vD, vD, vD, vS, vS, vS)
)
ClickSelectmoreOppositeBitmap = bitmap 20 20 color:vM (
	-- Soft corners
	setPixels ClickSelectmoreOppositeBitmap [0, 0] #(vG, vG)
	setPixels ClickSelectmoreOppositeBitmap [0, 1] #(vG)
	setPixels ClickSelectmoreOppositeBitmap [18, 0] #(vG, vG)
	setPixels ClickSelectmoreOppositeBitmap [19, 1] #(vG)
	setPixels ClickSelectmoreOppositeBitmap [0, 19] #(vG, vG)
	setPixels ClickSelectmoreOppositeBitmap [0, 18] #(vG)
	setPixels ClickSelectmoreOppositeBitmap [18, 19] #(vG, vG)
	setPixels ClickSelectmoreOppositeBitmap [19, 18] #(vG)
	-- graphics
	setPixels ClickSelectmoreOppositeBitmap [3, 8] #(vS, vS, vS, vM, vM, vM, vM, vM, vM, vM, vM, vS, vS, vS)
	setPixels ClickSelectmoreOppositeBitmap [2, 9] #(vS, vS, vS, vS, vS, vM, vM, vM, vW, vM, vM, vS, vS, vS, vS, vS)
	setPixels ClickSelectmoreOppositeBitmap [2, 10] #(vS, vS, vS, vS, vS, vM, vW, vW, vW, vW, vM, vS, vS, vS, vS, vS)
	setPixels ClickSelectmoreOppositeBitmap [2, 11] #(vS, vS, vS, vS, vS, vM, vM, vM, vW, vM, vM, vS, vS, vS, vS, vS)
	setPixels ClickSelectmoreOppositeBitmap [3, 12] #(vS, vS, vS, vM, vM, vM, vM, vM, vM, vM, vM, vS, vS, vS)
)

FreezeTransformBitmap = bitmap 20 20 color:vU (
	-- Soft corners
	setPixels FreezeTransformBitmap [0, 0] #(vG, vG)
	setPixels FreezeTransformBitmap [0, 1] #(vG)
	setPixels FreezeTransformBitmap [18, 0] #(vG, vG)
	setPixels FreezeTransformBitmap [19, 1] #(vG)
	setPixels FreezeTransformBitmap [0, 19] #(vG, vG)
	setPixels FreezeTransformBitmap [0, 18] #(vG)
	setPixels FreezeTransformBitmap [18, 19] #(vG, vG)
	setPixels FreezeTransformBitmap [19, 18] #(vG)
	-- graphics
	setPixels FreezeTransformBitmap [5, 5] #(vW, vW, vW, vW, vW, vW, vW, vW, vW)
	setPixels FreezeTransformBitmap [4, 6] #(vW, vU, vU, vU, vU, vU, vU, vU, vU, vU, vW)
	setPixels FreezeTransformBitmap [8, 8] #(vC, vC, vC)
	setPixels FreezeTransformBitmap [7, 9] #(vC, vC, vC, vC, vC)
	setPixels FreezeTransformBitmap [7, 10] #(vC, vC, vC, vC, vC)
	setPixels FreezeTransformBitmap [7, 11] #(vC, vC, vC, vC, vC)
	setPixels FreezeTransformBitmap [8, 12] #(vC, vC, vC)
	setPixels FreezeTransformBitmap [4, 14] #(vW, vU, vU, vU, vU, vU, vU, vU, vU, vU, vW)
	setPixels FreezeTransformBitmap [5, 15] #(vW, vW, vW, vW, vW, vW, vW, vW, vW)
)
OverFreezeTransformBitmap = bitmap 20 20 color:vD (
	-- Soft corners
	setPixels OverFreezeTransformBitmap [0, 0] #(vG, vG)
	setPixels OverFreezeTransformBitmap [0, 1] #(vG)
	setPixels OverFreezeTransformBitmap [18, 0] #(vG, vG)
	setPixels OverFreezeTransformBitmap [19, 1] #(vG)
	setPixels OverFreezeTransformBitmap [0, 19] #(vG, vG)
	setPixels OverFreezeTransformBitmap [0, 18] #(vG)
	setPixels OverFreezeTransformBitmap [18, 19] #(vG, vG)
	setPixels OverFreezeTransformBitmap [19, 18] #(vG)
	-- graphics
	setPixels OverFreezeTransformBitmap [5, 5] #(vW, vW, vW, vW, vW, vW, vW, vW, vW)
	setPixels OverFreezeTransformBitmap [4, 6] #(vW, vD, vD, vD, vD, vD, vD, vD, vD, vD, vW)
	setPixels OverFreezeTransformBitmap [8, 8] #(vC, vC, vC)
	setPixels OverFreezeTransformBitmap [7, 9] #(vC, vC, vC, vC, vC)
	setPixels OverFreezeTransformBitmap [7, 10] #(vC, vC, vC, vC, vC)
	setPixels OverFreezeTransformBitmap [7, 11] #(vC, vC, vC, vC, vC)
	setPixels OverFreezeTransformBitmap [8, 12] #(vC, vC, vC)
	setPixels OverFreezeTransformBitmap [4, 14] #(vW, vD, vD, vD, vD, vD, vD, vD, vD, vD, vW)
	setPixels OverFreezeTransformBitmap [5, 15] #(vW, vW, vW, vW, vW, vW, vW, vW, vW)
)
ClickFreezeTransformBitmap = bitmap 20 20 color:vM (
	-- Soft corners
	setPixels ClickFreezeTransformBitmap [0, 0] #(vG, vG)
	setPixels ClickFreezeTransformBitmap [0, 1] #(vG)
	setPixels ClickFreezeTransformBitmap [18, 0] #(vG, vG)
	setPixels ClickFreezeTransformBitmap [19, 1] #(vG)
	setPixels ClickFreezeTransformBitmap [0, 19] #(vG, vG)
	setPixels ClickFreezeTransformBitmap [0, 18] #(vG)
	setPixels ClickFreezeTransformBitmap [18, 19] #(vG, vG)
	setPixels ClickFreezeTransformBitmap [19, 18] #(vG)
	-- graphics
	setPixels ClickFreezeTransformBitmap [5, 5] #(vW, vW, vW, vW, vW, vW, vW, vW, vW)
	setPixels ClickFreezeTransformBitmap [4, 6] #(vW, vM, vM, vM, vM, vM, vM, vM, vM, vM, vW)
	setPixels ClickFreezeTransformBitmap [8, 8] #(vC, vC, vC)
	setPixels ClickFreezeTransformBitmap [7, 9] #(vC, vC, vC, vC, vC)
	setPixels ClickFreezeTransformBitmap [7, 10] #(vC, vC, vC, vC, vC)
	setPixels ClickFreezeTransformBitmap [7, 11] #(vC, vC, vC, vC, vC)
	setPixels ClickFreezeTransformBitmap [8, 12] #(vC, vC, vC)
	setPixels ClickFreezeTransformBitmap [4, 14] #(vW, vM, vM, vM, vM, vM, vM, vM, vM, vM, vW)
	setPixels ClickFreezeTransformBitmap [5, 15] #(vW, vW, vW, vW, vW, vW, vW, vW, vW)
)

ClipboardSelectBitmap = bitmap 20 20 color:vU (
	-- Soft corners
	setPixels ClipboardSelectBitmap [0, 0] #(vG, vG)
	setPixels ClipboardSelectBitmap [0, 1] #(vG)
	setPixels ClipboardSelectBitmap [18, 0] #(vG, vG)
	setPixels ClipboardSelectBitmap [19, 1] #(vG)
	setPixels ClipboardSelectBitmap [0, 19] #(vG, vG)
	setPixels ClipboardSelectBitmap [0, 18] #(vG)
	setPixels ClipboardSelectBitmap [18, 19] #(vG, vG)
	setPixels ClipboardSelectBitmap [19, 18] #(vG)
	-- graphics
	setPixels ClipboardSelectBitmap [9, 5] #(vW)
	setPixels ClipboardSelectBitmap [2, 6] #(vV, vV, vV, vV, vV, vU, vU, vU, vW)
	setPixels ClipboardSelectBitmap [10, 7] #(vW)
	setPixels ClipboardSelectBitmap [2, 8] #(vV, vV, vV, vV, vV, vU, vU, vU, vW, vU, vU, vU, vS, vS, vS)
	setPixels ClipboardSelectBitmap [10, 9] #(vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels ClipboardSelectBitmap [2, 10] #(vV, vV, vV, vV, vV, vU, vU, vU, vU, vW, vU, vS, vS, vS, vS, vS)
	setPixels ClipboardSelectBitmap [10, 11] #(vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels ClipboardSelectBitmap [2, 12] #(vV, vV, vV, vV, vV, vU, vU, vU, vW, vU, vU, vU, vS, vS, vS)
	setPixels ClipboardSelectBitmap [10, 13] #(vW)
	setPixels ClipboardSelectBitmap [2, 14] #(vV, vV, vV, vV, vV, vU, vU, vU, vW)
	setPixels ClipboardSelectBitmap [9, 15] #(vW)
)
OverClipboardSelectBitmap = bitmap 20 20 color:vD (
	-- Soft corners
	setPixels OverClipboardSelectBitmap [0, 0] #(vG, vG)
	setPixels OverClipboardSelectBitmap [0, 1] #(vG)
	setPixels OverClipboardSelectBitmap [18, 0] #(vG, vG)
	setPixels OverClipboardSelectBitmap [19, 1] #(vG)
	setPixels OverClipboardSelectBitmap [0, 19] #(vG, vG)
	setPixels OverClipboardSelectBitmap [0, 18] #(vG)
	setPixels OverClipboardSelectBitmap [18, 19] #(vG, vG)
	setPixels OverClipboardSelectBitmap [19, 18] #(vG)
	-- graphics
	setPixels OverClipboardSelectBitmap [9, 5] #(vW)
	setPixels OverClipboardSelectBitmap [2, 6] #(vV, vV, vV, vV, vV, vD, vD, vD, vW)
	setPixels OverClipboardSelectBitmap [10, 7] #(vW)
	setPixels OverClipboardSelectBitmap [2, 8] #(vV, vV, vV, vV, vV, vD, vD, vD, vW, vD, vD, vD, vS, vS, vS)
	setPixels OverClipboardSelectBitmap [10, 9] #(vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels OverClipboardSelectBitmap [2, 10] #(vV, vV, vV, vV, vV, vD, vD, vD, vD, vW, vD, vS, vS, vS, vS, vS)
	setPixels OverClipboardSelectBitmap [10, 11] #(vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels OverClipboardSelectBitmap [2, 12] #(vV, vV, vV, vV, vV, vD, vD, vD, vW, vD, vD, vD, vS, vS, vS)
	setPixels OverClipboardSelectBitmap [10, 13] #(vW)
	setPixels OverClipboardSelectBitmap [2, 14] #(vV, vV, vV, vV, vV, vD, vD, vD, vW)
	setPixels OverClipboardSelectBitmap [9, 15] #(vW)
)
ClickClipboardSelectBitmap = bitmap 20 20 color:vM (
	-- Soft corners
	setPixels ClickClipboardSelectBitmap [0, 0] #(vG, vG)
	setPixels ClickClipboardSelectBitmap [0, 1] #(vG)
	setPixels ClickClipboardSelectBitmap [18, 0] #(vG, vG)
	setPixels ClickClipboardSelectBitmap [19, 1] #(vG)
	setPixels ClickClipboardSelectBitmap [0, 19] #(vG, vG)
	setPixels ClickClipboardSelectBitmap [0, 18] #(vG)
	setPixels ClickClipboardSelectBitmap [18, 19] #(vG, vG)
	setPixels ClickClipboardSelectBitmap [19, 18] #(vG)
	-- graphics
	setPixels ClickClipboardSelectBitmap [9, 5] #(vW)
	setPixels ClickClipboardSelectBitmap [2, 6] #(vV, vV, vV, vV, vV, vM, vM, vM, vW)
	setPixels ClickClipboardSelectBitmap [10, 7] #(vW)
	setPixels ClickClipboardSelectBitmap [2, 8] #(vV, vV, vV, vV, vV, vM, vM, vM, vW, vM, vM, vM, vS, vS, vS)
	setPixels ClickClipboardSelectBitmap [10, 9] #(vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels ClickClipboardSelectBitmap [2, 10] #(vV, vV, vV, vV, vV, vM, vM, vM, vM, vW, vM, vS, vS, vS, vS, vS)
	setPixels ClickClipboardSelectBitmap [10, 11] #(vW, vU, vU, vS, vS, vS, vS, vS)
	setPixels ClickClipboardSelectBitmap [2, 12] #(vV, vV, vV, vV, vV, vM, vM, vM, vW, vM, vM, vM, vS, vS, vS)
	setPixels ClickClipboardSelectBitmap [10, 13] #(vW)
	setPixels ClickClipboardSelectBitmap [2, 14] #(vV, vV, vV, vV, vV, vM, vM, vM, vW)
	setPixels ClickClipboardSelectBitmap [9, 15] #(vW)
)

ParentToLastBitmap = bitmap 20 20 color:vU
OverParentToLastBitmap = bitmap 20 20 color:vD
ClickParentToLastBitmap = bitmap 20 20 color:vM

ParentToFirstBitmap = bitmap 20 20 color:vU
OverParentToFirstBitmap = bitmap 20 20 color:vD
ClickParentToFirstBitmap = bitmap 20 20 color:vM

SetViewportPrefsBitmap = bitmap 20 20 color:vU
OverSetViewportPrefsBitmap = bitmap 20 20 color:vD
ClickSetViewportPrefsBitmap = bitmap 20 20 color:vM

SkinTabBitmap = bitmap 35 20 color:vF
OverSkinTabBitmap = bitmap 35 20 color:vD
ClickSkinTabBitmap = bitmap 35 20 color:vU

CtrlTabBitmap = bitmap 35 20 color:vF
OverCtrlTabBitmap = bitmap 35 20 color:vD
ClickCtrlTabBitmap = bitmap 35 20 color:vU

RnmTabBitmap = bitmap 35 20 color:vF
OverRnmTabBitmap = bitmap 35 20 color:vD
ClickRnmTabBitmap = bitmap 35 20 color:vU

SpiritTabBitmap = bitmap 35 20 color:vF
OverSpiritTabBitmap = bitmap 35 20 color:vD
ClickSpiritTabBitmap = bitmap 35 20 color:vU
