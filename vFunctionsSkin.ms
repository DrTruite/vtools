/*
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of vTools.

vTools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vTools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with vTools.  If not, see <https://www.gnu.org/licenses/>.
*/

function copySkinOnSel = (
    /*
    Takes the weight of the fist vertex of the selection's bitarray and apply it on the rest of the vertex selection.
    It's kind of a gamble blend.
    */
    selectedVerts = #()
    for i =1 to (polyOp.getNumVerts selection[1]) where (skinOps.IsVertexSelected selection[1].modifiers[#Skin] i) do append selectedVerts i
    skinOps.selectedVertices selection[1].modifiers[#Skin] selectedVert[i]
    skinOps.copyWeights selection[1].modifiers[#Skin]
    skinOps.selectVertices selection[1].modifiers[#Skin] selectedVerts
    skinOps.pasteWeights selection[1].modifiers[#Skin]
)

function removeUnusedBones = (
    /*
    Remove bones that have no influence on any vertex.
    Must rewrite as original was taken from the internet
    */
    messagebox "Must rewrite as original was taken from the internet"
)

function reloadAllSkins = (
    /*
    Refresh the "Always deform" checkbox for each node with a Skin modifier in the scene
    */
    for obj in $* where classof obj == PolyMeshObject do (
        if (for modi in obj.modifiers where classOf modi == Skin collect modi).count > 0 do (
            max modify mode
            modPanel.setCurrentObject obj.modifiers[#Skin]
            obj.modifiers[#Skin].always_deform = off
            obj.modifiers[#Skin].always_deform = on
        )
    )
)

function autoScaleWeights = (
    /*
    For each selected vertices, set all weights relative to their distance to the skinning bones.
    */
    obj = selection[1]
    skinMod = obj.modifiers[#Skin]

    max modify mode
    modPanel.setCurrentObject skinMod
    subObjectLevel = 1

    vertexArray = for i=1 to (polyOp.getNumVerts obj) where (skinOps.IsVertexSelected skinMod i) == 1 collect i

    for ver=1 to vertexArray.count do (
        ctrlArray = for i=1 to (skinOps.getVertexWeightCount skinMod vertexArray[ver]) where (skinOps.getVertexWeight skinMod vertexArray[ver] i > 0) collect (skinOps.getVertexWeightBoneID skinMod vertexArray[ver] i)
        weightArray = #()
        sumDist = 0
        for b=1 to ctrlArray.count do (
            append weightArray (distance (polyOp.getVert obj vertexArray[ver]) (getNodeByName(skinOps.getBoneName skinMod ctrlArray[b] 0)).pos)
        )
        for b=1 to ctrlArray.count do (
            weightArray[b] = 1 / weightArray[b]
        )
        for b=1 to ctrlArray.count do (
            sumDist += weightArray[b]
        )
        for b=1 to ctrlArray.count do (
            weightArray[b] = weightArray[b] / sumDist
        )
        skinOps.ReplaceVertexWeights skinMod vertexArray[ver] ctrlArray weightArray
    )
)

function uniformizeWeights = (
    /*
    For each vertex in selection, uniformly distribute weight on each skinning bones.
    */
    selectedVertices = (for i=1 to (polyOp.getNumVerts selection[1]) where (skinOps.IsVertexSelected selection[1].modifiers[#Skin] i ) == 1 collect i)
    for vertID in selectedVertices do (
        ctrlArray = for i=1 to (skinOps.getVertexWeightCount selection[1].modifiers[#Skin] vertID) where (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i > 0) collect (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i)
        weightArray = for i=1 to ctrlArray.count collect (1.0 / ctrlArray.count)
        skinOps.ReplaceVertexWeights selection[1].modifiers[#Skin] vertID ctrlArray weightArray
    )
)

function gradientWeight = (
    /*
    Looks up global variables for input. Ugly:
        vertGradList : array of vertex IDs
    Creates a gradient of weight along the vertex array.
    */
    vertID = vertGradList[1]
    ctrlArrayA = for i=1 to (skinOps.getVertexWeightCount selection[1].modifiers[#Skin] vertID) where (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i > 0) collect (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i)
    weightArrayA = for i=1 to (skinOps.getVertexWeightCount selection[1].modifiers[#Skin] vertID) where (skinOps.GetVertexWeight selection[1].modifiers[#Skin] vertID i > 0) collect (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i)
    vertID = vertGradList[vertGradList.count]
    ctrlArrayB = for i=1 to (skinOps.getVertexWeightCount selection[1].modifiers[#Skin] vertID) where (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i > 0) collect (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i)
    weightArrayB = for i=1 to (skinOps.getVertexWeightCount selection[1].modifiers[#Skin] vertID) where (skinOps.GetVertexWeight selection[1].modifiers[#Skin] vertID i > 0) collect (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i)

    for j=1 to vertGradList.count do (
        joinedCtrlArray = ctrlArrayA + ctrlArrayB
        newWeightArray = #()
        for i=1 to weightArrayA.count do append newWeightArray (weightArrayA[i] * (1 - (j - 1.0) / (vertGradList.count - 1)))
        for i=1 to weightArrayB.count do append newWeightArray (weightArrayB[i] * ((j - 1.0) / (vertGradList.count - 1)))

        newCtrlArray = makeUniqueArray joinedCtrlArray
        if newCtrlArray.count != joinedCtrlArray.count then (
            newCtrlArray = joinedCtrlArray
            for i=1 to joinedCtrlArray.count do (
                for k=1 to joinedCtrlArray.count do (
                    if joinedCtrlArray[i] == joinedCtrlArray[k] and i != k do (
                        deleteItem newCtrlArray k
                        newWeightArray[i] = newWeightArray[i] + newWeightArray[k]
                        deleteItem newWeightArray k
                    )
                )
            )
        )
        skinOps.ReplaceVertexWeights selection[1].modifiers[#Skin] vertGradList[j] newCtrlArray newWeightArray
    )
)

function findMidPoint verts = (
    /*
    Takes an array of two vertex IDs.
    Does some kind of pathfinding to return a vertex located in the middle of them. If more than one are found, returns the closest on to the first vertex provided.
    */
    growListA = #{verts[1]}
    growListB = #{verts[2]}
    while (growListA * growListB).numberset <= 0 do (
        --grow
        growListA = polyOp.getVertsUsingEdge selection[1] (polyOp.getEdgesUsingVert selection[1] growListA)
        growListB = polyOp.getVertsUsingEdge selection[1] (polyOp.getEdgesUsingVert selection[1] growListB)
    )
    result = (growListA * growListB) - (verts as bitarray)
    if result.numberSet == 0 then result = #{verts[1]}
    else if result.numberSet == 1 then result = result
    else (
        growListC = #{verts[1]}
        while result.numberSet != 1 do (
            growListC = (polyop.getVertsUsingEdge selection[1] (polyOP.getEdgesUsingVert selection[1] growListC)) - #{verts[1]}
            if (result * growListC).numberSet == 1 then result = (result * growListC)
            else if (result * growListC).numberSet > 1 then ( -- diagonale
                distArray = for vert in (result * growListC) collect distance (polyOp.getVert selection[1] vert) (polyOp.getVert selection[1] verts[1])
                minIndex = 1
                for i=1 to distArray.count do if distArray[i] < distArray[minIndex] do minIndex = i
                result = #{((result * growListC) as array)[minIndex]}
            )
        )
    )
    result
)

function connectSelection verts = (
    /*
    Some kind of pathfinding.
    Takes a bitarray of vertexIDs.
    returns a bitArray of vertex IDs with both vertex plus every vertex in between them.
    */
    if (verts as array).count == 2 then (
        pointsToPath = verts as array
        pointsInPath = #()

        while pointsToPath.count > 1 do (
            --find middle
            workVerts = #(pointsToPath[1], pointsToPath[2])
            pointC = ((findMidPoint(workVerts)) as array)[1]
            if (#{pointC} * ((pointsToPath as bitarray) + (pointsInPath as bitarray))).numberset != 0 then (
                deleteItem pointsToPath 1
                append pointsInPath pointC
            )
            else (
                insertItem pointC pointsToPath 2
            )
        )
        --add the last point
        append pointsInPath pointsToPath[1]

        pointsInPath
    )
    else (
        format "Wrong number of vertices selected. % instead of 2\n" verts.numberset
        pointsInPath
    )
)

function findNearestVert vert loop = (
    /*
    Takes a vertex ID as well as a bitarray of vertex IDs.
    Returns the vertex ID from the bitarray that is the closest from the first vertex (topology-wise).
    If it finds more than one, it returns itself.
    */
    growList = #{vert}
    result = #{}
    while (growList * loop).numberset < 1 do (
        growList = polyOp.getVertsUsingEdge selection[1] (polyOp.getEdgesUsingVert selection[1] growList)
    )
    if (growList * loop).numberset == 1 then (
        result = ((growList * loop) as array)[1]
    )
    else (
        format "ERROR: Wrong number of intersections\n"
        result = vert
    )
    result
)

function weightToOneUnderCtrls obj thres:0.01 = (
    /*
    Takes a node. Optionnaly takes a threshold value.
    Parse all vertices in selection. If their position is the same as one of the bones in the modifier, it assigns this bone to vertex with a weight of 1.
    */
    threshold = thres
    skinMod = obj.modifiers[#Skin]

    max modify mode
    modPanel.setCurrentObject skinMod
    subObjectLevel = 1

    ctrlArray = for i=1 to (skinOps.getNumberBones skinMod) collect (getNodeByName (skinOps.getBoneName skinMod i 1))
    vertexArray = for i=1 to (polyOp.getNumVerts obj) where (skinOps.IsVertexSelected skinMod i) == 1 collect i

    for v in vertexArray do (
        for c=1 to ctrlArray.count do (
            if distance (polyOp.getVert obj v) (ctrlArray[c].transform[4]) < threshold do (
                skinOps.ReplaceVertexWeights skinMod v c 1
            )
        )
    )
)

function weightTo25UnderA4s obj thres:0.05 = (
    /*
    Takes a node. Optionnaly takes a threshold value.
    Parse all vertices in selection and grows the selection once.
    Takes all bones with the same coordinates as any of the grown vertex selection. Unifomely weight them to the vertex.
    */
    threshold = thres
    skinMod = obj.modifiers[#Skin]

    vertexArray = for i=1 to (polyOp.getNumVerts obj) where (skinOps.IsVertexSelected skinMod i) == 1 collect i

    for v in vertexArray do (
        vertSel = polyOp.getVertsUsingEdge obj (polyop.getEdgesUsingVert obj #{v})
        ctrlArray = #()
        for vert in vertSel do (
            for i=1 to (skinOps.getNumberBones skinMod) where ((distance (polyOp.getVert obj vert) (getNodeByName (skinOps.getBoneName skinMod i 1)).transform[4]) < threshold) do (
                appendIfUnique ctrlArray i
            )
        )
        weightArray = for i=1 to ctrlArray.count collect (1.0 / ctrlArray.count)
        skinOps.ReplaceVertexWeights skinMod v ctrlArray
    )
)

function averageAllWeights = (
    /*
    Does an average of all weight values between all selected vertices.
    Apply this average weight values to all selected vertices.
    */
    selectedVertices = (for i=1 to (polyOp.getNumVerts selection[1]) where (skinOps.IsVertexSelected selection[1].modifiers[#Skin] i ) == 1 collect i)
    newCtrlArray = #()
    newWeightArray = #()
    weightDivision = #()
    for vertID in selectedVertices do (
        for i=1 to (skinOps.getVertexWeightCount selection[1].modifiers[#Skin] vertID) where (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i > 0) do (
            if findItem newCtrlArray (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i) == 0 then (
                append newCtrlArray (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i)
                append newWeightArray (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i)
                append weightDivision 1
            )
            else (
                newWeightArray[findItem newCtrlArray (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i)] += (skinOps.getVertexWeight selection[1].modifiers[#Skin] vertID i)
                weightDivision[findItem newCtrlArray (skinOps.getVertexWeightBoneID selection[1].modifiers[#Skin] vertID i)] += 1
            )
        )
    )
    for i=1 to newWeightArray.count do newWeightArray[i] = newWeightArray[i] / weightDivision[i]
    for vertID in selectedVertices do (
        skinOps.ReplaceVertexWeights selection[1].modifiers[#Skin] vertID newCtrlArray newWeightArray
    )
)
