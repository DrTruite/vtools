/*
Copyright (C) 2019 Victor Chavanne <victor.chavanne@tutanota.com>

This file is part of vTools.

vTools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vTools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with vTools.  If not, see <https://www.gnu.org/licenses/>.
*/

function createBox = (
    /*
    Create a new box with the correct settings for controllers
    */
    newBox = box length:10 width:10 height:40
    newBox.boxmode = True
    newBox.renderable = off
    select newBox
    redrawViews()
)

function addBox = (
    /*
    Add a new box as a continuation of a FK chain after the selected box
    It will have the same properties as the previous box
    Script ends with the new box selected
    */
    for obj in selection where classOf obj == Box do (
        newBox = box length:obj.length width:obj.width height:obj.height wirecolor:obj.wirecolor
        newBox.boxmode = obj.boxmode
        newBox.parent = obj
        newBox.transform = obj.transform
        newBox.renderable = off
        in coordsys parent move newBox [0, 0, obj.height]
        select newBox
    )
    redrawViews()
)

function removeBox = (
    /*
    Delete selected boxes and select their parents
    */
    local boxesToDelete = #()
    local boxesToSelect = #()
    for obj in selection where classOf obj == Box do (
        append boxesToDelete obj
        if obj.parent != undefined then append boxesToSelect obj.parent
    )
    clearSelection()
    for obj in boxesToSelect do selectmore obj
    for obj in boxesToDelete do delete obj
    redrawViews()
)

function adjustHeight = (
    /*
    Set nodes height to the distance with their first child
    */
    for obj in selection where classOf obj == Box do (
        in coordsys parent obj.height = obj.children[1].pos[3]
    )
    redrawViews()
)

function saveHelperSizes = (
    /*
    Return array of the size for points of heights for boxes. Only works if all helpers are of the same class
    */
    sizeArray = #()
    allBoxes = True
    allPoints = True
    for obj in selection do (
        if classOf obj != Box then allBoxes = False
        if classOf obj != Point then allPoints = False
    )
    if allBoxes == True then (
        sizeArray = for obj in selection collect obj.height
    )
    else if allPoints == True then (
        sizeArray = for obj in selection collect obj.size
    )
    else (
        format "Wrong class of some/all nodes selected"
    )
    sizeArray
)

function scaleHeight svs h = (
    /*
    Takes an array of size (number) and a multiplier (number)
    Scale selected points or FK chain of boxes by the multiplier
    */
    if svs.count == 0 then svs = saveHelperSizes() -- saves helpers on first call
    allBoxes = True
    allPoints = True
    for obj in selection do (
        if classOf obj != Box then allBoxes = False
        if classOf obj != Point then allPoints = False
    )
    if allBoxes == True then (
        for i=1 to selection.count do (
            selection[i].height = svs[i] * h
            for j=1 to selection[i].children.count do (
                selection[i].children[j].pos = selection[i].pos
                in coordsys parent move selection[i].children[j] [0, 0, selection[i].height]
            )
        )
    )
    else if allPoints == True then (
        for i=1 to selection.count do (
            selection[i].size = svs[i] * h
        )
    )
    svs
)

function setWirecolor col = (
    /*
    Takes a colour. Set all nodes in selection's wirecolor to this colour.
    Why is this a function ? This mystery will challenge our minds for generations to come.
    */
    for s in selection do s.wirecolor = col
)

function createPointVerts = (
    /*
    Create a Point located on the barycenter of the selected vertices. Works with any subobject level other that 0
    If multiple objects are selected instead, create a point on the barycenter of their coordinates
    */
    if subObjectLevel == 0 or subObjectLevel == undefined then (
        finalPos = [0, 0, 0]
        for s in selection do finalPos += s.pos
        finalPos = finalPos / selection.count
        point pos:finalPos size:4
        redrawViews()
    )
    else if subObjectLevel >=1 then (
        suboblev = subObjectLevel
        if classOf selection[1] == PolyMeshObject or classOf selection[1] == Editable_Poly then selection[1].EditablePoly.ConvertSelection #CurrentLevel #Vertex
        subObjectLevel = 1
        selectedVerts = selection[1].selectedVerts
        selectedVertsCoordinates = #()
        if classOf selection[1] == PolyMeshObject or classOf selection[1] == Editable_Poly then (
            for vert in selectedVerts do append selectedVertsCoordinates (polyOp.getVert selection[1] vert.index)
        )
        else if classOf selection[1] == Editable_Mesh then (
            for verts in selectedVerts do append selectedVertsCoordinates (meshOp.getVert selection[1] vert.index)
        )
        else (
            message = "No behavious defined for " + ((classOf selection[1]) as string) + " objects"
            messagebox message
            append selectedVertsCoordinates [0, 0, 0]
        )
        finalPos = [0, 0, 0]
        for i=1 to selectedVerts.count do finalPos = finalPos + selectedVertsCoordinates[i]
        finalPos = finalPos / selectedVerts.count
        point pos:finalPos size:4
        subObjectLevel = suboblev
        redrawViews()
    )
)

function maxDistance points = (
    /*
    Takes an array of points IDs
    Return the maximum distance between any two points.
    Doesn't work as intended because the algorithm is highly flawed. Still does the trick so whatever.
    */
    maxDistanceValue = 0
    if points.count > 1 then (
        meanCoord = 0
        for i in points do meanCoord += polyOp.getVert selection[1] i
        meanCoord = meanCoord / points.count
        distanceArray = for i in points collect distance meanCoord (polyOp.getVert selection[1] i)
        sort distanceArray
        maxDistanceValue = distanceArray[distanceArray.count] + distanceArray[distanceArray.count - 1]
    )
    else (
        maxDistanceValue = 0
    )
    maxDistanceValue
)


function startEndBox btn startVals = (
    /*
    Takes a button
    First click will save the barycenter and maxDistance of the subobject coordinates
    Second click will save the barycenter and the maxDistance of the subobject coordinates
    It then creates a box between these two barycenters, with dimensions proportionals to the maxDistance
    */
    if btn.caption == "Start" then (
        if classof selection[1] == Point then (
            startPos = selection[1].position
            startWidth = selection[1].size
            btn.caption = "End"
        )
        else (
            suboblev = subObjectLevel
            selection[1].EditablePoly.ConvertSelection #CurrentLevel #Vertex
            subObjectLevel = 1
            selectedVerts = (polyOp.getVertSelection selection[1]) as array
            selectedVertsCoordinates = #()
            for vert in selectedVerts do append selectedVertsCoordinates (polyOp.getVert selection[1] vert)
            finalPos = [0, 0, 0]
            for i=1 to selectedVerts.count do finalPos = finalPos + selectedVertsCoordinates[i]
            finalPos = finalPos / selectedVerts.count
            startPos = finalPos
            startWidth = maxDistance(selectedVerts)
            subObjectLevel = suboblev
            btn.caption = "End"
        )
        #(startPos, startWidth)
    )
    else (
        if classOf selection[1] == Point then (
            endPos = selection[1].position
            endWidth = selection[1].size

            startPoint = Point pos:startVals[1]
            endPoint = Point pos:endPos

            startPoint.controller.rotation.controller = LookAt_Constraint()
            startPoint.controller.rotation.controller.appendTarget endPoint 100
            startPoint.controller.rotation.controller.target_axis = 2
            startPoint.controller.rotation.controller.pickUpNode = selection[1]
            startPoint.controller.rotation.controller.upNode_World = False

            theBox = Box length:(amax#(startVals[2], endWidth)) width:(amax#(startVals[2], endWidth)) height:(distance startVals[1] endPos)
            theBox.transform = startPoint.Transform

            delete startPoint
            delete endPoint

            theBox.boxMode = True
            theBox.renderable = off

            redrawViews()
            btn.caption = "Start"
        )
        else (
            suboblev = subObjectLevel
            selection[1].EditablePoly.ConvertSelection #CurrentLevel #Vertex
            subObjectLevel = 1
            selectedVerts = (polyOp.getVertSelection selection[1]) as array
            selectedVertsCoordinates = #()
            for vert in selectedVerts do append selectedVertsCoordinates (polyOp.getVert selection[1] vert)
            finalPos = [0, 0, 0]
            for i=1 to selectedVerts.count do finalPos = finalPos + selectedVertsCoordinates[i]
            finalPos = finalPos / selectedVerts.count
            endPos = finalPos
            endWidth = maxDistance(selectedVerts)
            subObjectLevel = suboblev

            startPoint = Point pos:startVals[1]
            endPoint = Point pos:endPos

            startPoint.controller.rotation.controller = LookAt_Constraint()
            startPoint.controller.rotation.controller.appendTarget endPoint 100
            startPoint.controller.rotation.controller.target_axis = 2

            theBox = Box length:(amax#(startVals[2], endWidth)) width:(amax#(startVals[2], endWidth)) height:(distance startVals[1] endPos)
            theBox.transform = startPoint.Transform

            delete startPoint
            delete endPoint

            theBox.boxMode = True
            theBox.renderable = off

            redrawViews()
            btn.caption = "Start"
        )
    )
)

function alignToMult objs transf:"position" = (
    /*
    Takes a node array. Place the first node in the barycenter of the rest of the nodes.
    If transf is set to "rotation", average the rotation instead
    */
    if transf == "position" then (
        finalPos = [0, 0, 0]
        for i=2 to objs.count do finalPos += objs[i].Position
        finalPos / (objs.count - 1)
    )
    else if transf == "rotation" then (
        m = transMatrix selection[1].transform[4]
        m[1] = [0, 0, 0]
        m[2] = [0, 0, 0]
        m[3] = [0, 0, 0]
        for i=2 to objs.count do (
            m[1] += objs[i].transform[1]
            m[2] += objs[i].transform[2]
            m[3] += objs[i].transform[3]
        )
        m[1] = (normalize m[1])
        m[2] = (normalize m[2])
        m[3] = (normalize m[3])
        m
    )
)

function resetDir objs axe = (
    /*
    Takes a node array. Takes an axis (as a number. 1 is X, 2 is Y, 3 is Z)
    Orient the first node as if it was on lookat on the second node. If a third node is provided, the first node will be positionned on it.
    */
    theUpNode = objs[1].parent
    if objs[3] != undefined do (
        objs[1].position = objs[3].position
        theUpNode = objs[3]
    )
    lookAtPoint = Point pos:(copy objs[1].position)

    lookAtPoint.parent = objs[1].parent
    in coordsys parent lookAtPoint.rotation = objs[1].rotation
    in coordsys parent lookAtPoint.position = objs[1].position
    objs[1].parent = lookAtPoint

    lookAtPoint.controller.rotation.controller = LookAt_Constraint()
    lookAtPoint.controller.rotation.controller.appendTarget objs[2] 100
    lookAtPoint.controller.rotation.controller.target_axis = ((mod (axe+2) 3) as integer)
    lookAtPoint.controller.rotation.controller.target_axisFlip = (axe > 3)
    lookAtPoint.controller.rotation.controller.pickUpNode = theUpNode
    lookAtPoint.controller.rotation.controller.upNode_World = false
    lookAtPoint.controller.rotation.controller.upNode_Axis = ((mod axe 3) as integer)
    lookAtPoint.controller.rotation.controller.StoUp_Axis = ((mod axe 3) as integer)

    objs[1].parent = lookAtPoint.parent
    delete lookAtPoint
    if classOf objs[1] == Box then (
        objs[1].height = distance objs[1] objs[2]
        if axe > 3 then objs[1].height *= -1
    )
    select objs[1]
)

function reconstructName nArray = (
    /*
    Takes a string array, add all the elements together with "_" inbetween and returns it as a string.
    */
    newName = nArray[1]
    if nArray.count > 1 then for i=2 to nArray.count do newName = newName + "_" + nArray[i]
    newName
)

function wiggleSelection theSel theAxis:"X" theMAxis:"" theAngle:20 theDistance:10.0 timeInc:10 = (
    /*
    Create a simple animation on selected nodes by rotating and translating them on the selected axis.
    Takes a node array.Takes two instances of one or more axis as string. Takes an angle value, a distance value and a time increment value (as numbers).
    Rotate each node along each axis, on at a time. Then move each node along each axis, one at a time. Each key timeInc frames appart.
    */
    sliderTime = 0f
    theTime = 0
    axis = [0, 0, 0]
    mAxis = [0, 0, 0]

    for objs in theSel do (
        for i=1 to theAxis.count do (
            if theAxis[i] == "X" then axis = [1, 0, 0]
            else if theAxis[i] == "Y" then axis = [0, 1, 0]
            else if theAxis[i] == "Z" then axis = [0, 0, 1]
            else (
                format "ERROR in wiggleSelection script: axis argument is set to '%'\n" theAxis
                format "should be using 'X' 'Y' or 'Z' only\n"
                format "using the local X axis instead\n"
                axis = [1, 0, 0]
            )

            set animate on
            at time theTime in coordsys local rotate obj (angle 0 axis)
            theTime += timeInc
            at time theTime in coordsys local rotate obj (angle theAngle axis)
            theTime += timeInc
            at time theTime in coordsys local rotate obj (angle (-theAngle * 2) axis)
            theTime += timeInc
            at time theTime in coordsys local rotate obj (angle theAngle axis)
            set animate off
        )

        for i=1 to theMAxis.count do (
            if theMAxis[i] == "X" then mAxis = [1, 0, 0]
            else if theMAxis[i] == "Y" then mAxis = [0, 1, 0]
            else if theMAxis[i] == "Z" then mAxis = [0, 0, 1]
            else (
                format "ERROR in wiggleSelection script: axis argument is set to '%'\n" theMAxis
                format "should be using 'X' 'Y' or 'Z' only\n"
                format "using the local X axis instead\n"
                mAxis = [1, 0, 0]
            )

            set animate on
            at time theTime in coordsys local move obj [0, 0, 0]
            theTime += timeInc
            at time theTime in coordsys local move obj (theDistance * mAxis)
            theTime += timeInc
            at time theTime in coordsys local move obj ((-theDistance * 2) * mAxis)
            theTime += timeInc
            at time theTime in coordsys local move obj (theDistance * mAxis)
            set animate off
        )
    )
)

function symmParent objs = (
    /*
    Takes a node array. Set the parent for the symmetric node to the symmetric node of the parent.
    */
    noParentSel = #()
    for obj in objs do (
        if (getOppositeNodes obj) == obj then format "% doesn't have a corresponding node to apply symmetry on.\n" obj.name
        else if obj.parent == undefined then append noParentSel obj
        else if findString obj.name "_l_" != undefined then (getNodeByName(substituteString obj.name "_l_" "_r_")).parent = getNodeByName(substituteString obj.parent.name "_l_" "_r_")
        else if findString obj.name "_r_" != undefined then (getNodeByName(substituteString obj.name "_r_" "_l_")).parent = getNodeByName(substituteString obj.parent.name "_r_" "_l_")
        else format "No symmetric object found fo %\n" obj.name
    )
    if noParentSel.count > 0 then (
        select noParentSel
        messagebox "Some nodes don't have parents. They have been selected for you"
    )
)

function getOppositeNodes vObjs = (
    /*
    Takes a node or an array of nodes.
    Return their symmetric nodes based on their naming. Either as an array of nodes or just a node.
    */
    result = #()
    objs = undefined

    if findItem #(Array, ObjectSet) (classOf vObjs) != 0 then objs = vObjs
    else objs = #(vObjs)

    for obj in objs do (
        if findString obj.name "_l_" != undefined then (
            if (getNodeByName (substituteString obj.name "_l_" "_r_")) != undefined then append result (getNodeByName (substituteString obj.name "_l_" "_r_"))
            else append result obj
        )
        else if findString obj.name "_r_" != undefined then (
            if (getNodeByName (substituteString obj.name "_r_" "_l_")) != undefined then append result (getNodeByName (substituteString obj.name "_r_" "_l_"))
            else append result obj
        )
        else (
            append result obj
        )
    )

    if findItem #(Array, ObjectSet) (classOf vObjs) == 0 then result = result[1]

    result
)

function checkWeightSetup = (
    /*
    THIS FUNCTION IS NOT UNDER THE GPL. I SHOULD REALLY REMOVE IT BEFORE PUBLISHING THIS. IF YOU COME ACROSS THIS PLEASE REMIND ME.
    Check all scene nodes for transform nodes named "Setup"
    If their weight value is not 100, prit their name.
    Probably written by Dom ? To rewrite my style.
    */
    clearListener()
    controllerList = #()
    join controllerList (getClassInstances Position_List)
    join controllerList (getClassInstances Rotation_List)

    for i=1 to controllerList.count do (
        if controllerList[i][2].name == "Setup" then (
            if (controllerList[i].weight[2]) != 100 then format "% -- setup weight -- select $%\n" controllerList[i].weight[2] (refs.dependentNodes controllerList[i])[1].name
        )
    )
)

function checkWeightSetupSym = (
    /*
    Check all scene nodes for transform nodes named "Setup"
    If their weight is not equal to the value of their symmetric node, print their name.
    */
    ControllerList = #()
    setupList = #()
    wrongPosList = #()
    wrongRotList = #()
    join controllerList (getClassInstances Position_List)
    join controllerList (getClassInstances Rotation_List)
    for i=1 to controllerList.count do (
        if controllerList[i][2].name == "Setup" do (
            appendIfUnique setupList (refs.dependentNodes controllerList[i])[1]
        )
    )

    while setupList.count > 0 do (
        if setupList[1].position.controller[2].name == "Setup" then (
            if setupList[1].position.controller[#Weights][2].value != (getOppositeNodes setupList[1]).position.controller[#Weights][2].value then (
                appendIfUnique wrongPosList (getOppositeNodes setupList[1])
                appendIfUnique wrongPosList setupList[1]
            )
        )
        if setupList[1].rotation.controller[2].name == "Setup" then (
            if setupList[1].rotation.controller[#Weights][2].value != (getOppositeNodes setupList[1]).rotation.controller[#Weights][2].value then (
                appendIfUnique wrongPosList (getOppositeNodes setupList[1])
                appendIfUnique wrongPosList setupList[1]
            )
        )
        try(deleteItem setupList (findItem setupList (getOppositeNodes setupList[1])))catch(format "ERROR: Coundn't find opposite node of % in the list\n" setupList[1].name)
        deleteItem setupList 1
    )

    if wrongPosList.count + wrongRotList.count != 0 then (
        if wrongPosList.count > 0 then (
            format "Some nodes have different weight values on their position tracks:\n"
            for elem in wrongPosList do (
                format "\t%\n" elem.name
            )
        )
        if wrongRotList.count > 0 then (
            format "Some nodes have different weight values on their rotation tracks:\n"
            for elem in wrongRotList do (
                format "\t%\n" elem.name
            )
        )
    )
    else (
        format "All weight values on position and rotation setup tracks are symmetrical\n"
    )
)

function detachAllElements = (
    /*
    Parse all elements in a polyObject and detach all of theim into different objects.
    */
    messageBox "This have not been written yet :("
)

function setArtsToZero = (
    /*
    Find all nodes with "_art_" in their name and set their position_script variables to 0
    */
    oldSel = selection as array
    select $*_ctrl_*_art*
    artList = selection as array
    if oldSel.count != 0 then select oldSel else clearSelection()
    for art in artList do (
        if art.position.controller[#Setup].VariableExists "posX" then art.position.controller[#Setup].SetConstant "posX" 0
        if art.position.controller[#Setup].VariableExists "posY" then art.position.controller[#Setup].SetConstant "posY" 0
        if art.position.controller[#Setup].VariableExists "posZ" then art.position.controller[#Setup].SetConstant "posZ" 0
        if art.position.controller[#Setup].VariableExists "negX" then art.position.controller[#Setup].SetConstant "negX" 0
        if art.position.controller[#Setup].VariableExists "negY" then art.position.controller[#Setup].SetConstant "negY" 0
        if art.position.controller[#Setup].VariableExists "negZ" then art.position.controller[#Setup].SetConstant "negZ" 0
    )
)

function checkUserDefSymmetry = (
    /*
    Check nodes with "_ctrl_" in their names.
    If the userDef properties of their symmetric nodes is different than theirs, print their name.
    */
    oldSel = selection as array
    select $*_ctrl_*
    ctrlList = selection as array
    if oldSel.count != 0 then select oldSel else clearSelection()
    returnArray = #()
    for ctrl in ctrlList do (
        if getUserPropBuffer ctrl != getUserPropBuffer (getOppositeNodes ctrl) do appendIfUnique returnArray ctrl.name
    )
    if returnArray.count == 0 then (
        format "All User Defined properties are symmetrical\n"
    )
    else (
        format "Some nodes are not symmetric:\n"
        for items in returnArray do format "\t%\n" item
    )
)

function addPointOnEachLoop obj ringA ringB = (
    /*
    Takes a node. Takes two vertex IDs array
    Add a point on the barycenter of each vertex loop found along the rings
    */
    for vertA in ringA do (
        vertB = findNearestVert vertA ringB
        edgeLoop = polyOp.getEdgesUsingVert obj #{vertA, vertB}
        for edges in edgeLoop do (
            if ((polyOp.getVertsUsingEdge obj #{edges}) - #{vertA, vertB}).numberSet == 0 then (
                edgeLoop = #{edges}
                exit
            )
        )
        polyOp.SetEdgeSelection obj edgeLoop
        obj.EditablePoly.SelectEdgeLoop()
        createPointVerts()
    )
)

function getSymmVertex obj axis:"X" thres:0.01 both:false = (
    /*
    Takes a node. Optionnaly takes an axis as a string, a threshold as a float and a boolean.
    Return a bitarray of vertex IDs whose coordinates are symmetric of the selected vertices along the axis provided.
    If "both" is set to True, returns the selected vertices as well.
    */
    thePivot = obj.pos
    oppositeVertices = #{}
    threshold = thres

    theAxis = [-1, 1, 1]
    if axis == "Y" then theAxis = [1, -1, 1]
    else if axis == "Z" then theAxis = [1, 1, -1]

    selectedVertices = #{}
    case classOf(modPanel.getCurrentObject()) of (
        Editable_Poly: selectedVertices = polyOp.getVertSelection obj
        Skin: selectedVertices = for i=1 to (polyOp.getNumVerts obj) where (skinOps.IsVertexSelected obj.modifiers[#Skin] i) == 1 collect i
        default: selectedVertices = #{}
    )

    for vert in selectedVertices do (
        vertCoord = polyOp.getVert obj vert
        vertSymmCoord = thePivot + ((vertCoord - thePivot) * thyeAxis)

        done = false
        v = 1
        while done == false do (
            if v > (polyOp.getNumVerts obj) then done = True
            else (
                if (distance (polyOp.getVert obj v) vertSymmCoord) < threshold then (
                    append oppositeVertices v
                    done = True
                )
                v += 1
            )
        )
    )

    if both == True then oppositeVertices = oppositeVertices + (selectedVertices as bitArray)
    oppositeVertices
)

function getSymmSub obj axis:"X" thres:0.01 both:false subOb:"Edge" = (
    /*
    Takes a node. Optionnaly takes an axis as a string, a threshold as a float, a boolean and a string describing the subobject nature (Edge or Face).
    Return a bitarray of IDs whose coordinates are symmetrics of the selected elements along the axis provided.
    If "both" is set ti true, returns the selected elements as well.
    */
    theAxis = axis
    theThres = thres
    if subOb == "Edge" then (
        oppositeEdges = #{}
        selectedEdges = polyOp.getEdgeSelection obj
        for edges in selectedEdges do (
            tempVerts = polyOp.getvfertsUsingEdge obj edges
            polyOp.setVertSelection obj tempVerts
            oppVerts = getSymmVertex obj
            edgeArrayA = polyOp.getEdgesUsingVert obj #{(oppVerts as array)[1]}
            edgeArrayB = polyOp.getEdgesUsingVert obj #{(oppVerts as array)[2]}
            inter = ((edgeArrayA * edgeArrayB) as array)
            if inter.count > 0 then append oppositeEdges inter[1]
            else messagebox "Coundn't find correct number of edges. May be caused by duplicate vertices.\n"
        )
        if both == true then oppositeEdges = oppositeEdges + selectedEdges
        oppositeEdges
    )
    else if subOb = "Face" then (
        oppositeFaces = #{}
        selectedFacs = polyOp.getFaceSelection obj
        for faces in selectedFaces do (
            tempVerts = polyOp.getvfertsUsingFace obj faces
            polyOp.setVertSelection obj tempVerts
            oppVerts = getSymmVertex obj
            faceArrayA = polyOp.getFacesUsingVert obj #{(oppVerts as array)[1]}
            faceArrayB = polyOp.getFacesUsingVert obj #{(oppVerts as array)[2]}
            faceArrayC = polyOp.getFacesUsingVert obj #{(oppVerts as array)[3]}
            inter = ((faceArrayA * faceArrayB * faceArrayC) as array)
            if inter.count > 0 then append oppositeFaces inter[1]
            else messagebox "Coundn't find correct number of faces. May be caused by duplicate vertices.\n"
        )
        if both == true then oppositeFaces = oppositeFaces + selectedFaces
        oppositeFaces
    )
    else (
        format "ERROR: Wrong subOb type. Script won't work. Should be 'Edge' or 'Face', not %\n" subOb
    )
)

function setViewportPrefs = (
    /*
    Set viewport settings the way Victor likes it.
    */
    viewport.setType #view_persp_user
    actionMan.executeAction 0 "310" -- Tools: Zoom Extents Selected
    (NitrousGraphicsManager.GetActiveViewportSettings()).showEdgedFacesEnabled = True
    for i=0 to (LayerManager.count - 1) do (
        (LayerManager.getLayer i).on = True
        (LayerManager.getLayer i).lock = False
    )
    (LayerManager.getLayerFromName ((filterString maxFileName "_")[1] + "_" + (filterString maxFileName "_")[2] + "_" + (filterString maxFileName "_")[3] + "_hide")).on = False
)

function roundCorner btmp col:(color 81 81 81) = (
    /*
    Takes a bitmap.
    Returns a bitmap
    Creates round corners on bitmap
    */
    setPixels btmp [0, 0] #(col, col)
    setPixels btmp [0, 1] #(col)
    setPixels btmp [btmp.width - 2, 0] #(col, col)
    setPixels btmp [btmp.width - 1, 0] #(col)
    setPixels btmp [0, btmp.height - 1] #(col, col)
    setPixels btmp [0, btmp.height - 2] #(col)
    setPixels btmp [btmp.width - 2, btmp.height - 1] #(col, col)
    setPixels btmp [btmp.width - 1, btmp.height - 2] #(col)
    btmp
)
